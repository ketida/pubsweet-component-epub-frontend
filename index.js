module.exports = {
  frontend: {
    actions: () => require('./actions'),
    reducers: () => require('./reducers')
  }
}
